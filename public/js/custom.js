$(function () {
    
	$.ajaxSetup({
	  	headers: {
	      	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	  	}
	});
});



$('body').on('click', '.deleteProduct', function () {
        var product_id = $(this).data("id");
        var delete_url = $(this).attr("href");

        if(confirm("Are You sure want to delete !")){
        	$.ajax({
	            type: "DELETE",
	            method: 'DELETE',
	            url: delete_url,
	            success: function (data) {
	                location.reload();
	            },
	            error: function (data) {
	            	location.reload();
	            }
	        });
        }
        return false;
      	
        
    });