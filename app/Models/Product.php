<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'price', 'description', 'user_id','image'
    ];

    public function users(){
        return $this->hasmany('App\User','id', 'user_id');
    }
    public function user(){
        return $this->hasOne('App\User','id','user_id');
    }
}
