<?php

namespace App\Http\Controllers\Products;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\User;
use Illuminate\Support\Facades\Validator;
use Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Product::with('users')->get();
        return view('product.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('product.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255', 'unique:products'],
            'description' => ['required',],
            'price' => ['required','regex:/^\d+(\.\d{1,2})?$/','between:0,99.99','max:4'],
            'file' => ['required','image','mimes:jpeg,png,jpg','max:2048'],
        ]);

        if ($request->file('file')) {
            $imagePath = $request->file('file');
            $imageName = rand(9999,999999).time().'.'.$request->file->extension();
            
            $path = $request->file->move(public_path('images'), $imageName);
            if($path){
                $createData = Product::create([
                    'name' => $request->name,
                    'price' => $request->price,
                    'description' => $request->description,
                    'image' => $imageName,
                    'user_id' => Auth::user()->id,
                ]);
                if($createData){
                    return redirect()->route('products.index')->with('success', 'Product Added Successfully.');
                }
                    return redirect()->route('products.create')->with('error', 'Product Not Added.');
            }
            return redirect()->route('products.create')->with('error', 'Product Not Added.');
        }
        return redirect()->route('products.create')->with('error', 'Product Not Added.');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id = decrypt($id);
        $data = Product::where('id',$id)->with('user')->first();
        return view('product.view',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = decrypt($id);
        $data = Product::where('id',$id)->with('user')->first();

        if($data->user_id !== (int)Auth::user()->id){
            return redirect()->route('products.index')->with('error','You have not permission to edit this product');
        }
        return view('product.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'description' => ['required'],
            'price' => ['required','regex:/^\d+(\.\d{1,2})?$/'],
        ]);
        $id = decrypt($id);
        $update = Product::find($id)->update([
            'name' => $request->name,
            'price' => $request->price,
            'description' => $request->description,
        ]);
        if($update){
            return redirect()->route('products.index')->with('success', 'Product Updated Successfully.');
        }
            return redirect()->route('products.create')->with('error', 'Product Not Added.');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = decrypt($id);
        $data = Product::where('id',$id)->with('user')->first();

        if($data->user_id !== (int)Auth::user()->id){
            return redirect()->route('products.index')->with('error','You have not permission to delete this product');
        }
        $data = Product::find($id)->delete();
        if($data){
            return redirect()->route('products.index')->with('success','Product deleted successfully.');
        }
        return redirect()->route('products.index')->with('error','Product not found.');
    }
}
