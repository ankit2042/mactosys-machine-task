@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                @if (\Session::has('error'))
                    <p class="alert alert-danger">
                        {!! \Session::get('error') !!}
                    </p>
                @endif
                <div class="card-header">{{ __('View Products Details') }}
                    <a href="{{ route('products.index') }}" class="btn btn-success btn-sm float-right">{{ __('Back') }}</a>
                </div>

                <div class="card-body">
                    <!--Section: Block Content-->
                    <div class="col-md-12">
                        <div class="col">
                            <div class="card col-md- 6shadow-sm"> 
                                <img src="{{ asset('images')}}/{{ $data->image }}" class="img-fluid mx-auto my-auto card-img-top" alt="...">
                                <div class="card-body text-center">
                                    <div class="clearfix mb-3"> 
                                        <span class="float-start badge rounded-pill bg-primary">{{ $data->name }}</span> 
                                        <span class="float-end price-hp">$ {{ $data->price }}</span> 
                                    </div>
                                    <h5 class="card-title border border-1 text-justify">{{ $data->description }}</h5>
                                    <div class="text-center my-4">
                                        <span>Created by : </span>
                                        <a href="#" class="btn btn-warning">{{ $data->users ? $data->users[0]->name : '' }}</a> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection