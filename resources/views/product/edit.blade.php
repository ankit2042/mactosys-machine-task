@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                @if (\Session::has('error'))
                    <p class="alert alert-danger">
                        {!! \Session::get('error') !!}
                    </p>
                @endif
                <div class="card-header">{{ __('View Products Details') }}
                    <a href="{{ route('products.index') }}" class="btn btn-success btn-sm float-right">{{ __('Back') }}</a>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('products.update',['product' => encrypt($data->id)]) }}">
                        @method('PUT')
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" readonly="" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $data->name }}"  autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Price') }}</label>

                            <div class="col-md-6">
                                <input id="price" type="text" class="form-control @error('price') is-invalid @enderror" name="price" value="{{ $data->price }}"  autocomplete="price">

                                @error('price')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>

                            <div class="col-md-6">
                                <textarea id="description" class="form-control @error('description') is-invalid @enderror" name="description" autocomplete="description">{{ $data->description }}</textarea>

                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="createdBy" class="col-md-4 col-form-label text-md-right">{{ __('Created By') }}</label>

                            <div class="col-md-6">
                                <input readonly="" id="createdBy" class="form-control @error('createdBy') is-invalid @enderror" name="createdBy" autocomplete="createdBy" value = "{{ $data->users ? $data->users[0]->name : '' }}" >

                                @error('createdBy')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update') }}
                                </button>
                                <a href="{{ route('products.index') }}" class="btn btn-danger">
                                    {{ __('Clear') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
