@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
            	@if (\Session::has('success'))
				    <div class="alert alert-success">
				        <ul>
				            <li>{!! \Session::get('success') !!}</li>
				        </ul>
				    </div>
				@endif
				@if (\Session::has('error'))
				    <div class="alert alert-danger">
				        <ul>
				            <li>{!! \Session::get('error') !!}</li>
				        </ul>
				    </div>
				@endif
            	<div class="card-header">
            		{{ __('Products List') }}
            		<a href="{{ route('products.create') }}" class="btn btn-success float-right">{{ __('Add Product') }}</a>
            	</div>
            	<div class="card-body">
            		<main>
					    <div class="container-fluid bg-trasparent my-4 p-3" style="position: relative;">
					        <div class="row row-cols-1 row-cols-xs-2 row-cols-sm-2 row-cols-lg-4 g-3">
					        	@forelse($data as $productKey => $productValue)
						            <div class="col">
						                <div class="card h-100 shadow-sm"> <img src="{{ asset('/images') }}/{{ $productValue->image }}" class="card-img-top" alt="...">
						                    <div class="card-body">
						                        <div class="clearfix mb-3"> 
						                        	<span class="float-start badge rounded-pill bg-primary">{{ $productValue->name }}</span> 
						                        	<span class="float-end price-hp">$ {{ $productValue->price }}</span> 
						                        </div>
						                        <h5 class="card-title text-justify">{{ $productValue->description }}</h5>
						                        <div class="text-center my-4">
						                        	<span>Created by :- </span> 
						                        	<a href="#" class="btn btn-warning btn-sm">{{ $productValue->users ? $productValue->users[0]->name : '' }} {{ date('d-m-Y', strtotime($productValue->created_at)) }} </a> </div>
						                        <a href="{{ route('products.show',['product' => encrypt($productValue->id) ]) }}" class="btn btn-secondary btn-sm">{{ __('View') }}</a>
									        	<a href="{{ route('products.edit', ['product' => encrypt($productValue->id) ]) }}" class="btn btn-success btn-sm">{{ __('Edit') }}</a>
									        	<a href="{{ route('products.destroy', ['product' => encrypt($productValue->id) ]) }}" class="btn btn-danger btn-sm deleteProduct" data-id="{{ encrypt($productValue->id) }}">{{ __('Delete') }}</a>
						                    </div>
						                </div>
						            </div>
						        @empty
						        	<p class="text-danger text-center">Data not found</p>
						        @endforelse
					        </div>
					    </div>
					</main>
            		<!-- <div class="table-responsive">
					  <table class="table">
					    <thead>
					      <tr>
					        <th scope="col">#</th>
					        <th scope="col">{{ __('Name') }}</th>
					        <th scope="col">{{ __('Price') }}</th>
					        <th scope="col">{{ __('Description') }}</th>
					        <th scope="col">{{ __('Created by') }}</th>
					        <th scope="col">{{ __('Actions') }}</th>
					      </tr>
					    </thead>
					    <tbody>
					    	@forelse($data as $productKey => $productValue)
						      <tr>
						        <th scope="row">{{$productKey+1}}</th>
						        <td>{{ $productValue->name }}</td>
						        <td>{{ $productValue->price }}</td>
						        <td>{{ $productValue->description }}</td>
						        <td>{{ $productValue->users ? $productValue->users[0]->name : '' }}</td>
						        <td>
						        	<a href="{{ route('products.show',['product' => encrypt($productValue->id) ]) }}" class="btn btn-secondary btn-sm">{{ __('View') }}</a>
						        	<a href="{{ route('products.edit', ['product' => encrypt($productValue->id) ]) }}" class="btn btn-success btn-sm">{{ __('Edit') }}</a>
						        	<a href="{{ route('products.destroy', ['product' => encrypt($productValue->id) ]) }}" id="{{encrypt($productValue->id)}}" class="btn btn-danger btn-sm deleteProduct">{{ __('Delete') }}</a>
						        {{ __('') }}</td>
						      </tr>
						     @empty
						     	<td colspan="6" class="text-danger text-center">Data not found</td>
						    @endforelse
					    </tbody>
					  </table>
					</div> -->
            	</div>
            </div>
        </div>
    </div>
</div>
@endsection